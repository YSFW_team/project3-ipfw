<!DOCTYPE>
<html>
<head>
<title>Fort Wayne Youth Sports Hub Email Notification Sign Up</title>
<link rel="stylesheet" type="text/css" href="includes/emailNotification.css">
<script src ="includes/utilities.js"></script>

<?php

    $user_name = "";
    $pass_word = "";

    $uError = '';
    $pError = '';
  
    // check if the form has been submitted
    if(isset($_POST['submitted']))
    { 
        // connect to the database
        require_once('../mysql_connection.php');

        // initialize an array to store errors
       // $errors = array();

        // validate first name
        if (empty($_POST['username']))
        {
            $uError = 'Username is required! ';
        }
        else
        {
            $user_name = mysqli_real_escape_string($db_connection , trim($_POST['username']));   
        }

        // validate last name
        if (empty($_POST['password']))
        {
             $pError = 'Password is required!';
        }
        else
        {
            $pass_word = mysqli_real_escape_string($db_connection, trim($_POST['password']));    

        }

        
        // if no error was detected
        if (empty($uError) && empty($pError)) 
        {
            // build sql query to be executed
            $sql_query = "INSERT INTO emailnotificationlist (FirstName, LastName, Email, Telephone, DateAndTime)
            VALUES ('$first_name', '$last_name', '$email_address', '$telephone_number', NOW())";

            // run the query
            $result = @mysqli_query($db_connection, $sql_query);

            // if the query is successful
            if($result)
            {
                echo '<h1>Thank you!</h1>
                <p>You are now registered to receive sport events email notifications</p>
                <p><br /></p>';
            }
            else 
            { // if record was not created successfully
                echo '<h1>System Error</h1>
                <p class ="error">We could not log you in due to system error. 
                Please contact your administrator. We appologyze for any incovenience.</p>';

                // debug error message
                echo "<p>" . mysqli_error($db_connection) . "</p>";
               // . "<br /><br />Query: " . $sql_query . "</p>";

            } // end if ($command)

            // close database connection
            mysqli_close($db_connection);

            // terminate the script
            exit();
        } 
        else
        { // report the errors
            echo '<h2 class ="error">System Error!</h2>';
            
        } // end of if(empty(errors))

    } // end of if(isset($_POST['submitted'])), main submit conditional

    function login($username, $password, $mysqli) 
    {
   
    if ($stmt = $mysqli->prepare("SELECT username, password, 
        FROM users
       WHERE email = ?
        LIMIT 1")) 
    {
        $stmt->bind_param('s', $email);  // Bind "$email" to parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($user_id, $username, $db_password, $salt);
        $stmt->fetch();
 
        // hash the password with the unique salt.
        $password = hash('sha512', $password . $salt);
        if ($stmt->num_rows == 1) {
            // If the user exists we check if the account is locked
            // from too many login attempts 
 
            if (checkbrute($user_id, $mysqli) == true) {
                // Account is locked 
                // Send an email to user saying their account is locked
                return false;
            } else {
                // Check if the password in the database matches
                // the password the user submitted.
                if ($db_password == $password) {
                    // Password is correct!
                    // Get the user-agent string of the user.
                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    // XSS protection as we might print this value
                    $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                    $_SESSION['user_id'] = $user_id;
                    // XSS protection as we might print this value
                    $username = preg_replace("/[^a-zA-Z0-9_\-]+/", 
                                                                "", 
                                                                $username);
                    $_SESSION['username'] = $username;
                    $_SESSION['login_string'] = hash('sha512', 
                              $password . $user_browser);
                    // Login successful.
                    return true;
                } else {
                    // Password is not correct
                    // We record this attempt in the database
                    $now = time();
                    $mysqli->query("INSERT INTO login_attempts(user_id, time)
                                    VALUES ('$user_id', '$now')");
                    return false;
                }
            }
        } else {
            // No user exists.
            return false;
        }
    }
}

       
    ?>




?>

</head>
    <body>
        <div id ="container" style ="margin-left: 60px;">

        <div id ="header">
            <img border="0" alt="index_02" src="images/index_02.gif" width="938" height="77">
            <img border="0" alt="tweeter logo" src="images/index_04.gif" width="79" height="77">
            <img border="0" alt="facebook logo" src="images/index_06.gif" width="78" height="77">
            <img border="0" alt="rss logo" src="images/index_08.gif" width="101" height="77">
        </div>

        <div id ="logo">
            <img border="0" alt="FYSH logo" src="images/index_10.gif" width="1208" height="209">
        </div>

        <div id ="navigation">
            <img border="0" alt="soccer" src="images/index_25.gif" width="171" height="58">
            <img border="0" alt="basketball" src="images/index_26.gif" width="184" height="58">
            <img border="0" alt="football" src="images/index_27.gif" width="149" height="58">
            <img border="0" alt="tennis" src="images/index_28.gif" width="140" height="58">
            <img border="0" alt="baseball" src="images/index_29.gif" width="159" height="58">
            <img border="0" alt="about us" src="images/index_30.gif" width="175" height="58">
            <img  border="0" alt="contact us" src="images/index_31.gif" width="206" height="58" style="padding:0px;">>
        </div>

        <div style="border-width: 2px; border-style: solid; border-color: #633f3f;  
              margin-right: 120px; margin-bottom: 4px; padding-left: 25px;  
              padding-top: 15px; width: 1180; alt="signup form">
              <h1>Login</h1>
            <form name ="signupForm" action="handleSignup.php" method="POST">
               <!-- <fieldset> -->
                   <legend>login</legend><br />
                   <span class ="error">* Required fields</span><br /><br />
        
                    Username:<br />
                    <input id ="uname" type="text" name="username"> 
                    <span class ="error"><?php echo "$uError" ?></span><br /><br />
        
                    Password:<br />
                    <input id ="pw" type="password" name="pass">
                    <span class ="error"><?php echo "$pError" ?></span><br /><br />
          
                    <input type="submit" name = "submitted" value="Login">
               <!-- </fieldset> -->
            </form>
        </div>
        <div id ="footer">
            <img border="0" alt="contact us" src="images/index_38.gif" width="1208" height="65">
        </div>

    </body>
</html>