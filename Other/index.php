
<!DOCTYPE>
<html>
<head>
<title>Fort Wayne Youth Sports Hub Email Notification Sign Up</title>
<link rel="stylesheet" type="text/css" href="emailNotification.css">
<script></script>

</head>
    <body>

    <?php
            // define db access
    DEFINE ('DB_USER', "'honopac'");
    DEFINE ('DB_PASSWORD', 'h76m6499');
    DEFINE ('DB_HOST', 'localhost');
    DEFINE ('DB_NAME', 'acs567');

    // variables
    $first_name = "";      
    $last_name = "";       
    $email_address = "";       
    $telephone_number = "";

    $firstnameError = "";
    $lastnameError = "";
    $emailError = "";
    $telephoneError = "";
    $error = false;

    // get data submitted
    if(isset($_POST["signupForm"]))
    { 
        // validate first name
        if (empty($_POST["firstname"]))
        {
            $firstnameError = "First name is required!";
            $error = true;
    
        }
        else
        {
            $first_name = test_input($_POST["firstname"]);     

        }

        // validate last name
        if (empty($_POST["lastname"]))
        {
            $lastnameError = "Last name is required!";
            $error = true;
        }
        else
        {
            $last_name = test_input($_POST["lastname"]);       
        }

        // validate email address
        if (empty($_POST["email"]) | !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL))
        {
            $emailError = "Valid email address is required!";
            $error = true;
        }
        else
        {
            $email_address = test_input($_POST["email"]);      
        }

        // validate telephone number
        if(empty($_POST["telephone"]) | (!preg_match("/^[0-9]{10}$/", test_input($_POST["telephone"]))))
        {
            $telephoneError = "Valid telephone number is required!";
            $error = true;
        }
        else
        {

            $telephone_number = test_input($_POST["telephone"]);     
        }
    }
       
        // Create connection
        $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

        //Check connection
        if ($conn->connect_error) 
        {
           die("Connection failed: " . $conn->connect_error);
        } 

        // Check if any of the variables is "" string 
        if(empty($first_name) ||
            empty($last_name) ||
            empty($email_address) ||
            empty($telephone_number))
            {
                // Set all variables to NULL so that the database creates a 
                // null insertion error when empty string is inserted to the db
                $first_name = null;
                $last_name = null;
                $email_address = null;
                $telephone_number = null;
            }

           $sql = "INSERT INTO emailnotificationlist (FirstName, LastName, Email, Telephone, DateAndTime)
           VALUES ('$first_name', '$last_name', '$email_address', '$telephone_number', NOW())";

           // $sql = "INSERT INTO emailnotificationlist (FirstName, LastName, Email, Telephone, DateAndTime)
           // VALUES ('null', 'null', 'null', 'null', NOW()";

            if ($conn->query($sql) === TRUE) 
            {
                echo "New record created successfully";
            } 
            else 
            {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }

            $conn->close();

    // test user input for any cross-site scripting attack
    function test_input($input)
    {
        $vetted_input = trim($input);
        $vetted_input = stripslashes($input);
        $vetted_input = htmlspecialchars($input);
    
        return $vetted_input;
    }

    ?>

        <div>
            <form name ="signupForm" action="index.php" method="POST" onsubmit="return validateForm();">
    	        <fieldset>
		           <legend>Sign Up to Receive our Email Newsletter</legend><br />
                   <span class ="error">* Required fields</span><br /><br />
        
                    First name:<br />
		            <input id ="defaultText" type="text" name="firstname" placeholder ="Honore">
                    <span class="error">* <?php echo $firstnameError;?></span><br />
        
		            Last name:<br />
		            <input id ="lastname" type="text" name="lastname" placeholder ="Hodary">
                    <span class="error">* <?php echo $lastnameError;?></span><br /> 
        
                    Email:<br />
		            <input id ="email" type="text" name="email" placeholder ="honorehodary@gmail.com">
                    <span class="error">* <?php echo $emailError;?></span><br />
        
	                Telephone:<br />
		            <input id ="telephone" type="text" name="telephone" placeholder ="2604568970">
                  <span class="error">* <?php echo $telephoneError;?></span><br /><br />
                    <input type="submit" value="Submit">
                </fieldset>
            </form>
        </div>
    </body>
</html>
