
<!DOCTYPE>
<html>
<head>
<title>Fort Wayne Youth Sports Hub Email Notification Sign Up</title>
<link rel="stylesheet" type="text/css" href="includes/emailNotification.css">
<script src ="includes/utilities.js"></script>

<?php

    // variables
    $first_name = "";      
    $last_name = "";       
    $email_address = "";       
    $telephone_number = "";

    $fError = '';
    $lError = '';
    $eError = '';
    $tError = '';

    $pattern = '/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/';

    // check if the form has been submitted
    if(isset($_POST['submitted']))
    { 
        // connect to the database
        require_once('../mysql_connection.php');

        // initialize an array to store errors
       // $errors = array();

        // validate first name
        if (empty($_POST['firstname']))
        {
            $fError = 'First name is required! ';
        }
        else
        {
            $first_name = mysqli_real_escape_string($db_connection , trim($_POST['firstname']));   
        }

        // validate last name
        if (empty($_POST['lastname']))
        {
             $lError = 'Last name is required!';
        }
        else
        {
            $last_name = mysqli_real_escape_string($db_connection, trim($_POST['lastname']));    

        }

        // validate email address
        if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) 
        {
             $eError = 'Valid email address is required!';
        }
        else
        {
            $email_address = mysqli_real_escape_string($db_connection, trim($_POST['email']));           
        }

        // validate telephone number
        if(empty($_POST['telephone']) || !preg_match('/^[2-9]{1}[0-9]{9}$/', $_POST['telephone']))
        {
             $tError = 'Valid telephone number is required!';
        }
        else
        {
            $telephone_number = mysqli_real_escape_string($db_connection, trim($_POST["telephone"]));     
        }

        // if no error was detected
        if (empty($fError) && empty($lError) && empty($eError) && empty($tError)) 
        {
            // build sql query to be executed
            $sql_query = "INSERT INTO newsletter (FirstName, LastName, Email, Telephone, DateAndTime)
            VALUES ('$first_name', '$last_name', '$email_address', '$telephone_number', NOW())";

            // run the query
            $result = @mysqli_query($db_connection, $sql_query);

            // if the query is successful
            if($result)
            {
                /*
                echo '<h1>Thank you!</h1>
                <p>You are now registered to receive sport events newsletters</p>
                <p><br /></p>'; */

                echo '<script type="text/javascript">
                window.alert("Thank you. You are now registered to receive sport events newsletters");
                window.location.href = "handleSignup.php";
                </script>';
            }
            else 
            { // if record was not created successfully
                /*
                echo '<h1>System Error</h1>
                <p class ="error">We could not sign you up for sport events email notification due to system error. 
                We appologyze for any incovenience.</p>'; */

                echo'<script type="text/javascript">
                alert("We could not sign you up for sport events email notification due to system error. 
                We appologyze for any incovenience");
                window.location.href = "handleSignup.php";
                </script>';

                // debug error message
                echo "<p>" . mysqli_error($db_connection) . "</p>";
               // . "<br /><br />Query: " . $sql_query . "</p>";

            } // end if ($command)

            // close database connection
            mysqli_close($db_connection);

            // terminate the script
            exit();
        } 

        /*
        else
        { // report the errors
            echo '<h2 class ="error">System Error!</h2>';
            
        } // end of if(empty(errors))
        */

    } // end of if(isset($_POST['submitted'])), main submit conditional
       
    ?>

</head>
    <body>
        <div id ="container" style ="margin-left: 60px;">

        <div id ="header">
            <img border="0" alt="index_02" src="images/index_02.gif" width="938" height="77">
            <img border="0" alt="tweeter logo" src="images/index_04.gif" width="79" height="77">
            <img border="0" alt="facebook logo" src="images/index_06.gif" width="78" height="77">
            <img border="0" alt="rss logo" src="images/index_08.gif" width="101" height="77">
        </div>

        <div id ="logo">
            <img border="0" alt="FYSH logo" src="images/index_10.gif" width="1208" height="209">
        </div>
        <div id ="navigation">
            <a href="handleSignup.php"><img border="0" alt="Signup for Newsletter" src="images/signup.gif" width="214" height="58"></a>
            <a href="soccerpage.html"><img border="0" alt="soccer" src="images/index_25.gif" width="140" height="58"></a>
            <a href="basketballpage.html"><img border="0" alt="basketball" src="images/index_26.gif" width="140" height="58"></a>
            <a href="footballpage.html"><img border="0" alt="football" src="images/index_27.gif" width="140" height="58"></a>
            <a href="tennispage.html"><img border="0" alt="tennis" src="images/index_28.gif" width="140" height="58"></a>
            <a href="baseballpage.html"><img border="0" alt="baseball" src="images/index_29.gif" width="135" height="58"></a>
            <a href="aboutuspage.html"><img border="0" alt="about us" src="images/index_30.gif" width="135" height="58"></a>    
            <a href="contactuspage.html"><img  border="0" alt="contact us" src="images/index_31.gif" width="136" height="58" style="padding:0px;"></a>
            
        </div>

        <div style="border-width: 2px; border-style: solid; border-color: #633f3f;  
              margin-right: 120px; margin-bottom: 4px; padding-left: 25px;  
              padding-top: 15px; width: 1180; alt="signup form">
            <form name ="signupForm" action="handleSignup.php" method="POST">
               <!-- <fieldset> -->
                   <h3>Sign Up to Receive our Email Newsletter</h3>
                   <!-- <span class ="error">* Required fields</span><br /><br /> -->
        
                    First name:<br />
                    <input id ="defaultText" type="text" name="firstname" placeholder ="Honore">
                    <span class ="error"><?php echo "$fError" ?></span><br /><br />
        
                    Last name:<br />
                    <input id ="lastname" type="text" name="lastname" placeholder ="Hodary">
                    <span class ="error"><?php echo "$lError" ?></span><br /><br />
        
                    Email:<br />
                    <input id ="email" type="text" name="email" placeholder ="honorehodary@gmail.com">
                    <span class ="error"><?php echo "$eError" ?></span><br /><br />
        
                    Telephone:<br />
                    <input id ="telephone" type="text" name="telephone" placeholder ="260-456-8970">
                    <span class ="error"><?php echo "$tError" ?></span><br /><br />
                  
                    <input type="submit" name = "submitted" value="Submit">
               <!-- </fieldset> -->
            </form>
        </div>
        <div id ="footer">
            <img border="0" alt="contact us" src="images/index_38.gif" width="1208" height="65">
        </div>

    </body>
</html>
