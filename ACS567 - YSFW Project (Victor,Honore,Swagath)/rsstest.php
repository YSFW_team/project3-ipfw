<!DOCTYPE html>

<html>
  <head>
    <title>Fort Wayne Sports RSS Reader</title>
	<style>
	a {
		color: #FF0000;
	}
</style>
  </head>
  <body>
    <h1>Fort Wayne Sports </h1>
    <ul>
      <?php

         $dom = simplexml_load_file("http://www.fortwayne.com/apps/pbcs.dll/section?template=RSS&profile=1002&mime=xml&site=FW");
         foreach ($dom->channel->item as $item)
         {
             $time = strtotime($item->pubDate);
             $date = date("M j, Y", $time);
             print "<li>";
             print "<a href='{$item->link}'>";
             print $item->title;
             print "</a>";
             print " (" . $date . ")";
			 print "<p>";
			 echo $item->description;
			 print "</p>";
             print "</li>";
         }

      ?>
    </ul>
  </body>
</html>