function validateForm()
{
	var firstName = document.forms["signupForm"]["firstname"].value;
	var lastName = document.forms["signupForm"]["lastname"].value;
	var emailAddress = document.forms["signupForm"]["email"].value;
    var telephoneNumber = document.forms["signupForm"]["telephone"].value;
    

    if (firstName == null || firstName == "") 
    {
        alert("First name must be filled out");
        document.signupForm.firstname.focus() ;
        
        return false;
    }

    if (lastName == null || lastName == "") 
    {
        alert("Last name must be filled out");
        document.signupForm.lastname.focus() ;
        
        return false;
    }

    if (emailAddress == null || emailAddress == "" || !(checkEmail(emailAddress)) 
    {
        alert("Email address must be filled out");
        document.signupForm.email.focus() ;
        
        return false;
    }

    if (telephoneNumber == null || telephoneNumber == "") 
    {
        alert("Telephone number must be filled out");
        document.signupForm.telephone.focus() ;
        
        return false;
    }

    return true;
}

function checkEmail(inputvalue)
{    
    var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;

     return pattern.test(inputvalue));
}